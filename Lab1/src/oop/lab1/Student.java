package oop.lab1;

//TODO Write class Javadoc
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals(Object other) {
		if(other.getClass().getSimpleName().equals("Student")){
			Student newStudent = (Student) other;
			return (id == (newStudent.id));
		}
		if(other.getClass().getSimpleName().equals("Person")){
			Person newPerson = (Person) other;
			return super.equals(newPerson);
		}
		return false;
	}
}
